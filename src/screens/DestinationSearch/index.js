import React, { useEffect, useState } from "react";
import { View, TextInput, SafeAreaView } from "react-native";

import styles from './styles'
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
const DestinationSearch = (props) => {

    const [originPlace, setOriginPlace] = useState(null);
    const [destinationPlace, setDestinationPlace] = useState(null);

    useEffect(() => {
        if (originPlace && destinationPlace) {
            console.warn('Redirect to results');
        }

    }, [originPlace, destinationPlace])

    return (
        <SafeAreaView>
            <View style={styles.container}>       
                <GooglePlacesAutocomplete
                    placeholder='Where from?'
                    onPress={(data, details = null) => {
                        setOriginPlace({data, details});
                    }}
                    styles={{
                        textInput: styles.textInput,
                    }}
                    fetchDetails
                    query={{
                        key: 'AIzaSyBxuPZUzs_80cLPfKBivmqcRgJMAdPO_dA',
                        language: 'en',
                    }}
                />

                <GooglePlacesAutocomplete
                    placeholder='Where to?'
                    onPress={(data, details = null) => {
                        setDestinationPlace({data, details});
                    }}
                    styles={{
                        textInput: styles.textInput,
                    }}
                    fetchDetails
                    query={{
                        key: 'AIzaSyBxuPZUzs_80cLPfKBivmqcRgJMAdPO_dA',
                        language: 'en',
                    }}
                />

            </View>
        </SafeAreaView>
    )
}

export default DestinationSearch;